import redis
import face_recognition
from face_recognition.face_recognition_cli import image_files_in_folder
import numpy as np
import cv2
import time
import base64
import os
import pickle
import json
import math
import itertools
from multiprocessing import Manager
from ImgTempService import RedisBrain
from multiprocessing import Manager
from imutils.video import VideoStream
from loggers import logger
from configuration import KEY_CAMERA_RGB, KEY_CAMERA_IR, KEY_CAMERA_IR_ROW, TRAINED_MODEL_NAME, PI_FRAME_SIZE_WEIGHT, PI_FRAME_SIZE_HEIGHT


Global = Manager().Namespace()
Global.known_person = []
Global.known_face_encodings = []

def load_known_faces():
	
	try:
		with open(TRAINED_MODEL_NAME, "rb") as face_data_file:
			dict_name_embeddings =  pickle.load(face_data_file)
			Global.known_person = [*dict_name_embeddings]
			Global.known_face_encodings = [*dict_name_embeddings.values()]
			size_of_each_person_embeddings = [len(x) for x in Global.known_face_encodings]
			Global.known_face_encodings = list(itertools.chain.from_iterable(Global.known_face_encodings))
			Global.known_person = list(itertools.chain.from_iterable((itertools.repeat(item, cnt) for item, cnt in zip(Global.known_person,size_of_each_person_embeddings ))))
	except FileNotFoundError as e:
		print("No previous face data found - starting with a blank known face list.")
		pass

def save_known_faces(train_dir, model_save_path=None, verbose = True):

	# The training data would be all the face encodings from all the known images and the labels are their names
	encodings = []
	names = []
	all_face_encodings = {}

	# Loop through each person in the training set
	for class_dir in os.listdir(train_dir):
		if not os.path.isdir(os.path.join(train_dir, class_dir)):
			continue

		logger.info("Training {} image".format(class_dir))
		encodings = []
		# Loop through each training image for the current person
		for img_path in image_files_in_folder(os.path.join(train_dir, class_dir)):
			image = face_recognition.load_image_file(img_path)
			face_bounding_boxes = face_recognition.face_locations(image, model="cnn")

			if len(face_bounding_boxes) != 1:
				# If there are no people (or too many people) in a training image, skip the image.
				if verbose:
					print("Image {} not suitable for training: {}".format(img_path, "Didn't find a face" if len(face_bounding_boxes) < 1 else "Found more than one face"))
			else:
				# Add face encoding for current image to the training set
				encodings.append(face_recognition.face_encodings(image, known_face_locations=face_bounding_boxes)[0])
				names.append(class_dir)

		all_face_encodings[class_dir] = encodings

	# Save the trained KNN classifier
	if model_save_path is not None:
		with open(model_save_path, 'wb') as f:
			pickle.dump(all_face_encodings, f)


def get_face_names(rgb_small_frame, face_locations, Global):
	# Find all the faces and face encodings in the current frame of video
	face_encodings = face_recognition.face_encodings(face_image=rgb_small_frame, known_face_locations=face_locations, model="large")

	face_names = []
	for face_encoding in face_encodings:
		# See if the face is a match for the known face(s)
		name = "Visitor"

		face_distances = face_recognition.face_distance(Global.known_face_encodings, face_encoding)
		best_match_index = np.argmin(face_distances)
		if face_distances[best_match_index] < 0.65:
			name = Global.known_person[best_match_index]

		face_names.append(name)

	return face_locations, face_names

def compute_temp_from_frame(frame_process, lepton_frame_process, face_locations):
	temp = 36.5
	for face_location in face_locations:
		for (top, right, bottom, left) in face_location:
			#(YU, XR, YD, XL)
			#(y0, x0, y1, x1)
			right, top = camera_distortion(x=right, y=top)
			left, bottom = camera_distortion(x=left, y=bottom)
			box = np.array([top, right, bottom, left])
			(top, right, bottom, left) = box.astype("int")
			roi = lepton_frame_process[top:bottom, left:right]
			flatten_roi = roi.flatten()
			if not flatten_roi.size:
				continue
			temp = np.sort(flatten_roi)[-5:].mean()
			temp = round(temp, 2)
	
	return temp

def camera_distortion(x, y, thermal_shape = (160, 120), h = 0.9, v = 1.012, im_x=PI_FRAME_SIZE_WEIGHT, im_y=PI_FRAME_SIZE_HEIGHT):
	#@https://github.com/cliffordlab/AutoTriage_release/blob/cd2610c8e14c185d19011708036c15c71ec3b64a/forehead_detection/detect_forehead.py#L10
	## camera fov factor:
	# horizontal: np.tan(57/2*np.pi/180)/np.tan(31.1*np.pi/180) = 0.9
	# diagonal of lepton: 71degree, vertical=np.sqrt(71.3293**2 -54.295**2)=46.259
	# vertical: 0.46259/np.tan(24.4*np.pi/180) = 1.012
	## Since every variable is fixed
	# x_ratio = (320*0.9)/160 = 1.8
	# y_ratio = (240*1.012)/120 = 2.024
	# x_bias = (320*0.9 - 320)/2 = -16
	# y_bias = (240*1.012 - 240)/2 = 1.44
	## Variable pattern
	#x_ratio = (im_x*h)/thermal_shape[0] 
	#y_ratio = (im_y*v)/thermal_shape[1]
	#x_bias = (im_x*h-im_x)/2
	#y_bias = (im_y*v-im_y)/2
	x_ratio = 1.8
	y_ratio = 2.024
	x_bias = -16
	y_bias = 1.44
	x = ( x + x_bias) /x_ratio 
	y = ( y + y_bias) /y_ratio
	return int(x), int(y)

def convertCvNumpyByteTOString(img_bytes):
	#https://stackoverflow.com/questions/8908287/why-do-i-need-b-to-encode-a-string-with-base64
	return base64.b64encode(img_bytes).decode("ascii")

def main():
	"""
	1. 抓取 redis server 上的 RGB 影像
	2. 抓取 redis server 上的 Temp 影像
	3. 更新已知員工圖片資料夾
	4. BBOX, image, name= 人臉比對(unknow_face, known_faces)
	5. 傳送 {BBOX, image, name} 到 redis server
	"""

	r = RedisBrain()
	r.connect()

	logger.info('Init Face recognition model services')

	isRecognizing = False
	while True:
		time.sleep(1)
		face_names = "UNKNOW"
		face_locations = (0, 0, 0, 0)
		Names_img_array = ""
		temp = 36.53

		img_bytes = r.get('Camera_RGB') #'Camera_RGB', 'Camera_IR', 'Camera_IR_Row'
		if img_bytes is None and type(img_bytes) is not 'bytes':
			raise Exception("Must be btyes format from redis")

		img_array = np.frombuffer(img_bytes, np.uint8)
		image = cv2.imdecode(img_array, cv2.IMREAD_COLOR)
		small_frame = cv2.resize(image, (0, 0), fx=0.25, fy=0.25)
		X_face_locations = face_recognition.face_locations(small_frame, number_of_times_to_upsample=1, model="cnn")
		# If no faces are found in the image, return an empty result.
		if len(X_face_locations) != 0 and isRecognizing == False:
			print("Recogniting .....")
			isRecognizing = True
			face_locations, face_names = get_face_names(small_frame, X_face_locations, Global)

			resized_data = r.get('Camera_IR_Row')#bytes
			Names_img_array = np.frombuffer(resized_data, np.float64) #(19200,dtype:float64)
			Names_img_array = np.reshape(Names_img_array, newshape=(160, 120))
			temp = compute_temp_from_frame(image, Names_img_array, [X_face_locations])

			if temp < 33 or math.isnan(temp):
				isRecognizing = False
				continue
			print(X_face_locations)
			print(face_names)
			print(temp)

			isRecognizing = False

if __name__ == '__main__':
	save_known_faces("./train_dataset", model_save_path=TRAINED_MODEL_NAME)
	load_known_faces()
	main()