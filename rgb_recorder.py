import cv2
import time
import numpy as np
from imutils.video import VideoStream
from ImgTempService import RedisBrain
from loggers import logger
from configuration import KEY_CAMERA_RGB, PUBSUB_CAMERA_RGB, PI_FRAME_SIZE_WEIGHT, PI_FRAME_SIZE_HEIGHT

class VideoCamera(object):

	def __init__(self):
		logger.info('Init camera configuration.')
		self.usingPiCamera = False
		self.frameSize = (PI_FRAME_SIZE_WEIGHT, PI_FRAME_SIZE_HEIGHT)
		self.video = VideoStream(usePiCamera=self.usingPiCamera, resolution=self.frameSize,framerate=24).start()
		time.sleep(2.0)
	
	def __del__(self):
		logger.info('Closing RGB video...')
		self.video.stop()
	
	def get_frame(self):
		image = self.video.read()
		# time.sleep(0.5)
		return image

def main():
	r = RedisBrain()
	r.connect()

	RGB_camera = VideoCamera()
	value = 0
	image = 0
	## Capture and store
	while True:
		# Inspire from github
		image = RGB_camera.get_frame()
		image = cv2.resize(image, (PI_FRAME_SIZE_WEIGHT, PI_FRAME_SIZE_HEIGHT))
		hello, image = cv2.imencode('.jpg', image)
		ndarray = np.array(image) #dtype = uint8, shape=(20600, 1)
		value = ndarray.tobytes()
		r.set(
			KEY_CAMERA_RGB, 
			value
			)
		r.publish(
			PUBSUB_CAMERA_RGB,
			value
		)

if __name__ == '__main__':
	main()