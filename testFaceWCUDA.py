import face_recognition
import time

sum = 0
N = 100

start = time.time()

for i in range(N):
    image = face_recognition.load_image_file("./train_dataset/humans.jpg")
    face_locations = face_recognition.face_locations(image, number_of_times_to_upsample=0, model="cnn")
end = time.time()
sum = end - start
print("CUDA version: {}".format(sum/N))

sum = 0
N = 100

start = time.time()

for i in range(N):
    image = face_recognition.load_image_file("./train_dataset/humans.jpg")
    face_locations = face_recognition.face_locations(image)

end = time.time()
sum = end - start
print("Without CUDA version: {}".format(sum/N))