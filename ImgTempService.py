import time
import redis
import traceback
from loggers import logger
from configuration import REDIS_URL, REDIS_PORT, MAX_CONNECTION


class RedisBrain(object):
    def __init__(self):
        self.redis = None
        self.subscriber = None

    def connect(self):
        if not REDIS_URL:
            logger.info('No ImgTempService on this attendance machine.')
            return

        logger.info('ImgTempService Connecting...')
        try:
            pool = redis.ConnectionPool(
                host=REDIS_URL, port=REDIS_PORT,
                max_connections=MAX_CONNECTION, db=0
            )
            self.redis = redis.Redis(connection_pool=pool)
            self.redis.set('ping', 'pong')
            self.subscriber = self.redis.pubsub()
            logger.info('ImgTempService Connected: {}'.format(REDIS_URL))
            logger.info('ImgTempService Connected get : {}'.format(self.redis.get('ping')))
        except Exception as e:
            logger.error(traceback.format_exc())
            raise e

    def set(self, key, value):
        if not self.redis:
            return False

        self.redis.set(key, value)
        return True

    def get(self, key):
        if not self.redis:
            return None

        value = self.redis.get(key)
        return value

    def lpush(self, key, value):
        if not self.redis:
            return False

        return self.redis.lpush(key, value)

    def lpop(self, key):
        if not self.redis:
            return None

        value = self.redis.lpop(key)
        return value
    
    def hmset(self, key , dic):
        #bytes, string, int or float first
        if not self.redis:
            return False
        
        return self.redis.hmset(key, dic)
    
    def hmget(self, key , key_list):
        #bytes, string, int or float first
        if not self.redis:
            return False
        
        return self.redis.hmget(key, key_list)
    
    def hget(self, key , value):
        if not self.redis:
            return False
        
        return self.redis.hget(key, value)
    
    def publish(self, channel, value):
        if not self.redis:
            return False
        
        return self.redis.publish(channel, value)
    
    def subscribe(self, channel):
        if not self.redis:
            return False
        
        subscriber = self.redis.pubsub()
        return subscriber.subscribe(channel)