"""
Serve webcam images from a Redis store using Tornado.
Usage:
   python server.py
"""

import base64
import time
import datetime
import coils
import redis
import cv2
import os
import numpy as np
import face_recognition
from io import StringIO
import json
import socketio
from PIL import Image
from ImgTempService import RedisBrain
from loggers import logger
from tornado import websocket, web, ioloop, autoreload, escape, gen
from tornado.escape import json_encode
from configuration import TORANDO_PORT, KEY_CAMERA_RGB, KEY_CAMERA_IR
from rx import  operators as ops
from rx.subject import Subject, BehaviorSubject
import rx


## 12/19測試 socketio 包裝成的 server bj6kc6g3m/4
#https://github.com/miguelgrinberg/python-socketio/issues/201
sio = socketio.AsyncServer(async_mode="tornado")
_Handler = socketio.get_tornado_handler(sio)
r = RedisBrain()
r.connect()

##
redisStreamData = BehaviorSubject(None)
redisListenStream = Subject()
redisListenStream.pipe(
    ops.filter(lambda message: message['name'] is not redisStreamData.value['name'])
).subscribe(
    lambda x: redisStreamData.on_next(x)
)

class IndexHandler(web.RequestHandler):
	""" Handler for the root static page. """
	def get(self):
		self.render('index.html')

class TestViewHandler(web.RequestHandler):
	""" Handler for the root static page. """
	def get(self):
		self.render('testView.html')

class SocketHandler(websocket.WebSocketHandler):
	""" Handler for the websocket URL. """
	
	def __init__(self, *args, **kwargs):
		""" Initialize the Redis store and framerate monitor. """

		super(SocketHandler, self).__init__(*args, **kwargs)
		self._store = r

	def on_message(self, message):
		""" Retrieve image ID from database until different from last ID,
		then retrieve image, de-serialize, encode and send to client. """

		image = self._store.get(KEY_CAMERA_RGB)
		image = base64.b64encode(image)
		self.write_message(image)

class IRSocketHandler(websocket.WebSocketHandler):
	""" Handler for the websocket URL. """
	
	def __init__(self, *args, **kwargs):
		""" Initialize the Redis store and framerate monitor. """

		super(IRSocketHandler, self).__init__(*args, **kwargs)
		self._store = r

	def on_message(self, message):
		""" Retrieve image ID from database until different from last ID,
		then retrieve image, de-serialize, encode and send to client. """

		image = self._store.get(KEY_CAMERA_IR)
		image = base64.b64encode(image)
		self.write_message(image)

def main():
	app = web.Application(
		[
			(r'/', IndexHandler),
			(r'/testView', TestViewHandler),
			(r'/websocket', SocketHandler),
			(r'/IRwebsocket', IRSocketHandler),
		],
		#cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
		template_path=os.path.join(os.path.dirname(__file__), "templates"),
		static_path=os.path.join(os.path.dirname(__file__), "static"),
		#xsrf_cookies=True,
		#debug=options.debug,
		debug=True,
		gzip=True,
	)
	app.listen(TORANDO_PORT)
	logger.info("Tornado server listen on {}".format(TORANDO_PORT))
	
	ioloop.IOLoop.current().start()
	logger.info("Tornado server start")

if __name__ == "__main__":
	main()