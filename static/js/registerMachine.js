//function DoAjax(){
//    $(document).ready(function() {
//        $('button').click(function(event) {
//            console.log("click From Js");
//        });
//    });
//}
$(document).ready(function(){
    if ("WebSocket" in window) {
       var ws_protocol = 'ws://'
       var ws_channel = 'IRwebsocket'
       //var ws_path =  ws_protocol + window.location.host + window.location.pathname + ws_channel;
       var ws_path =  ws_protocol + window.location.host + '/' + ws_channel;
       var ws = new WebSocket (ws_path);
       //alert(ws);
       ws.onopen = function () {
          console.log("Connection to ", ws_path)
          ws.send(1);
       };
       ws.onmessage = function (msg) {
          $("#ir_cam").attr('src', 'data:image/jpg;base64,' + msg.data);
          ws.send(1.5);
       };
       ws.onerror = function (e) {
          console.log(e);
          ws.send(1);
       };
    } else {
          alert("WebSocket not supported");
    }
 });