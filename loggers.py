import logging

# generic logger
level = logging.DEBUG
logger = logging.getLogger('SBIR')
logger.setLevel(level)

log_file_handler = logging.FileHandler('SBIR.log')
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
log_file_handler.setFormatter(formatter)
logger.addHandler(log_file_handler)

level = logging.DEBUG
temp_lepton_dht22_logger = logging.getLogger('SBIR')
temp_lepton_dht22_logger .setLevel(level)

log_file_handler = logging.FileHandler('temp_lepton_dht22.log')
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
log_file_handler.setFormatter(formatter)
temp_lepton_dht22_logger .addHandler(log_file_handler)