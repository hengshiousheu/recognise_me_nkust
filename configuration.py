# Torando server
TORANDO_PORT = 9000

# number of redis pool
MAX_CONNECTION = 20

# set redis url if you want to enable redis related functions like redis_brain
REDIS_URL = '0.0.0.0'    # loecalhost
REDIS_PORT = 6379    # 6379

# set redis key-value list name
KEY_CAMERA_RGB = 'Camera_RGB'
KEY_CAMERA_IR = 'Camera_IR'
KEY_CAMERA_IR_ROW = 'Camera_IR_Row'

PUBSUB_CAMERA_RGB = 'PUBSUB_Camera_RGB'
PUBSUB_CAMERA_IR = 'PUBSUB_Camera_IR'
PUBSUB_CAMERA_IR_ROW = 'PUBSUB_Camera_IR_Row'

#PI camera configuration
PI_FRAME_SIZE_WEIGHT = 320
PI_FRAME_SIZE_HEIGHT = 240

#Lepton camera configuration
LEPTON_FRAME_SIZE_WEIGHT = 320
LEPTON_FRAME_SIZE_HEIGHT = 240

#Trained_model_name
TRAINED_MODEL_NAME = 'known_faces.dat'

