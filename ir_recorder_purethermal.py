import cv2
import time
import base64
import numpy as np
import subprocess
from ImgTempService import RedisBrain
from loggers import logger
from PIL import Image
from configuration import LEPTON_FRAME_SIZE_WEIGHT, LEPTON_FRAME_SIZE_HEIGHT, KEY_CAMERA_IR, KEY_CAMERA_IR_ROW, PUBSUB_CAMERA_IR, PUBSUB_CAMERA_IR_ROW

class LeptonCamera(object):

	def __init__(self):
		logger.info('Init Lepton Camera configuration.')
		self.frameSize = (LEPTON_FRAME_SIZE_WEIGHT, LEPTON_FRAME_SIZE_HEIGHT)
		self.rotate_90_times = 2
		cmd = "gst-launch-1.0 v4l2src num-buffers=-1 device=/dev/video1 ! video/x-raw,format=GRAY16_LE ! pnmenc ! multifilesink location=./ir/ir.pnm".split()
		self.FLIR = subprocess.Popen(cmd)
		time.sleep(2)

	def __del__(self):
		self.FLIR.kill()

	def _ktoc(self, val):
		# 16-bit Kelvin to deg Celsius
		return (val - 27315) / 100.0

	def _raw_to_8bit(self, data):
		"""
		:Desc: # normalize to 0-255 # convert to uint8
		:param data: 
		:return:
		"""
		cv2.normalize(data, data, 0, 255, cv2.NORM_MINMAX)
		return np.uint8(data)

	def get_frame(self):
		self.data = np.array(Image.open('./ir/ir.pnm'))[:120,:]
		resized_data = self._ktoc(self.data[:,:])
		frame = self._raw_to_8bit(resized_data)
		ret, jpeg = cv2.imencode('.jpg', frame)
		return jpeg

	def get_row_ir_data(self):
		self.data = np.array(Image.open('./ir/ir.pnm'))[:120,:]
		resized_data = self._ktoc(self.data[:,:])#(120, 160, numpy.array)
		return resized_data

	def calibration(inputTemp):
		temp = 0.0113 * inputTemp - 313.383
		return temp

def main():
	logger.info('Init Redis server')
	r = RedisBrain()
	r.connect()
	logger.info('Init Redis server done')
	
	logger.info('Start Capturing IR video...')
	lepton_camera = LeptonCamera()
	## Capture and store
	while True:
		# For Lepton IR
		lepton_frame = lepton_camera.get_frame()
		value = np.array(lepton_frame).tobytes()
		r.set(KEY_CAMERA_IR, value)
		r.publish(PUBSUB_CAMERA_IR,
			value
		)

		# For Leptin IR Row
		lepton_frame = lepton_camera.get_row_ir_data() #numarray, float64
		lepton_frame_str = lepton_frame.tostring() #bytes
		r.set(KEY_CAMERA_IR_ROW, lepton_frame_str)
		r.publish(PUBSUB_CAMERA_IR_ROW,
			lepton_frame_str
		)

if __name__ == '__main__':
	main()