# 體溫偵測人臉辨識系統
本專案用於 高雄科技大學(NKUST)-ARLab 做使用，可以辨識出進場人員是誰，以及當下體溫為何。

## 架構圖
本圖片紀錄考勤機存在哪些角色，角色之間溝通的方式，以及各自該處理的方式

## 硬體設備
 - [Nvidia jetson Xavier AGX](https://www.nvidia.com/zh-tw/autonomous-machines/jetson-agx-xavier/) 
 - [Webcam](https://store.logitech.tw/v2/official/SalePageCategory/326229?sortMode=Curator)
 - [FLIP Lepton 3.5](https://ricelee.com/lepton-lwir-micro-thermal-camera-module)
 - [pureThermal 2](https://www.digikey.tw/product-detail/zh/groupgets-llc/PURETHERMAL-2/2077-PURETHERMAL-2-ND/9866290?utm_adgroup=Dev%20Boards&utm_source=google&utm_medium=cpc&utm_campaign=Smart%20Shopping_Product_Development%20Boards&utm_term=&productid=9866290&gclid=Cj0KCQiA3smABhCjARIsAKtrg6IVzDgk35wgiW3xcCYTE97DvIHS2QS8rXFQqE331nsZksaJciPpzisaAk4-EALw_wcB)
 - [many wire adapter](https://24h.pchome.com.tw/prod/DCAX4G-A9008KA15)

## 資料夾結構說明
 - torando_server: 用於顯示網頁用的 web server。
 - ImgTempService: 與 Redis Server 溝通用
 - ir_recorder: 與 FLIP Lepton 3.5 溝通，將資料傳送給 Redis Server
 - rgb_recorder: 與 PiCamera 8mp 溝通，將資料傳送給 Redis Server
 - face_recognition_model: 用於進行辨識人臉，以及計算出體溫，並將結果顯示於 command line
 - loggers: 紀錄 torando server 發現一切，方便追蹤
 - configuration: 各種設定，如 ip address, port 的使用等

## 硬體建置環境
 - 使用 [Jetson SDK manager](https://developer.nvidia.com/nvidia-sdk-manager) 安裝基礎環境
 - Plug webcam first in the back of Xavier.
 - Next plug pureThermal in the front of Xavier.

## 軟體環境建置
 - 強烈建議安裝 [jetson_stats](https://github.com/rbonghi/jetson_stats)
 - 本專案用到 face_recognition，須先自行編譯 [dlib](https://github.com/davisking/dlib)，請依照 github 指示操作，編譯順利會看到使用可以使用 CUDA
 - 本專案用到 Redis，建議自行編譯過 [Redis](https://redis.io/)

## 操作流程

1. 啟用 Redis-server 先
```
$ redis-server
```
2. 測試 Redis 確實開啟
```
$ redis-cli
$ ping
```
3. 開啟 WebCam, 來傳送 RGB 影像
```
$ python3 rgb_recorder.py
```
4. 開啟 Lepton 3.5, 來傳送 Temp 影像
```
$ python3 ir_recorder_purethermal.py
```
5. 開啟 face_recognition_model 來進行訪客辨識，還有體溫偵測
```
$ python3 face_recognition_model.py
```
6. 開啟 Torando server 來開啟考勤機服務
```
python3 torando_server.py
```
7. (options) 開始網頁 ip，0.0.0.0:9000/testView 查看是否有 RGB & IR 影像畫面

## 對外連接 websocket channel 紀錄
[WebSocket] ws://<IP_ADDRESS>:<PORT>/<WS_CHANNEL>
1. /websocket: 取得 RGB mjpeg 影像
2. /IRwebsocket: 取得 TEMP mjpeg 影像

## 除錯機制
1. gst-launch 出現 /dev/videoX 錯誤時，代表 USB 洞錯誤。
```
執行 以下指令來查看可用 video 洞
$ v4l2-ctl --list-devices
```
2. 使用以下方式來查看 dlib 是否有使用到 GPU 進行運算
```
$ python3 
>>>import dlib
>>>dlib.DLIB_USE_CUDA
```
## 改進方向
本專案僅使用到一點點點 Xavier AGX 能力，未來應該使用 NVIDIA pre-trained model，配合內建硬解 library 來達到效果

- [ ] 使用 jetson.utils.videoSource() 作為影像輸入源頭
- [ ] 使用 jetson.inference.detectNet() 作為人臉偵測
- [ ] 熟悉 cudaImage 與 cv2 與 ndarray 之間的轉換
