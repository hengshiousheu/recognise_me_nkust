import cv2
import time
import numpy as np
from ImgTempService import RedisBrain
from loggers import logger
import platform
from configuration import KEY_CAMERA_RGB, PUBSUB_CAMERA_RGB, PI_FRAME_SIZE_WEIGHT, PI_FRAME_SIZE_HEIGHT

import jetson.inference
import jetson.utils

import argparse
import sys

class XavierCamera(object):

	def __init__(self):
		self.input_URI = "/dev/video0"
		self.network = "ssd-mobilenet-v2"
		self.overlay = "box,labels,conf"
		self.threshold = 0.5
		self.input = jetson.utils.videoSource(self.input_URI, argv=sys.argv)

	def __del__(self):
		pass

	def get_frame(self):
		img = self.input.Capture()
		return img

def main():
	# logger.info('Init Redis server')
	r = RedisBrain()
	r.connect()
	# logger.info('Init Redis server done')
	RGB_camera = XavierCamera()
	output = jetson.utils.videoOutput("display://0")
	## Capture and store

	
	while True:
		# Inspire from github
		image = RGB_camera.get_frame()
		cuda_img = jetson.utils.cudaAllocMapped(width=int(image.width), height=int(image.height), format='rgb32f')
		ndarray = jetson.utils.cudaToNumpy(cuda_img)
		print("ndarray")
		print(ndarray.dtype)
		print(ndarray.shape)
		print(type(ndarray))
		value = ndarray.tobytes()

		# Demo how to read from numpy?
		img_array = np.frombuffer(value, np.uint8)
		cude_mem  = jetson.utils.cudaFromNumpy(img_array)

		# Demo how to render the picture on the screen
		output.Render(image)

		r.set(
			KEY_CAMERA_RGB, 
			value
		)
		r.publish(
			PUBSUB_CAMERA_RGB,
			value
		)

if __name__ == '__main__':
	main()